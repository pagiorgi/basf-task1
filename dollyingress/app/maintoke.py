#packages and modules
from fastapi import FastAPI
import torch
import time
from app.modules.instruct_pipeline import InstructionTextGenerationPipeline
from transformers import AutoModelForCausalLM, AutoTokenizer


tokenizer = AutoTokenizer.from_pretrained("databricks/dolly-v2-3b", padding_side="left")
model = AutoModelForCausalLM.from_pretrained("databricks/dolly-v2-3b", device_map="auto", torch_dtype=torch.bfloat16)
generate_text = InstructionTextGenerationPipeline(model=model, tokenizer=tokenizer)


# API
app = FastAPI()


@app.get("/")
def dollytoke(q):
    start = time.time()
    res = generate_text(q)
    end = time.time()
    time_interval = end - start
    return {"query": q, "respons": res[0]["generated_text"], "time": time_interval}