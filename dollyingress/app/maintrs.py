#packages and modules
from fastapi import FastAPI
import torch
import time
from transformers import pipeline


# trust_remote_code=True
generate_text_trs = pipeline(model="databricks/dolly-v2-3b",torch_dtype=torch.bfloat16,trust_remote_code=True,device_map="auto") #22 second

# API
app = FastAPI()

@app.get("/")
def dollytrs(q):
    start = time.time()
    res = generate_text_trs(q)
    end = time.time()
    time_interval = end - start
    return {"query": q, "respons": res[0]["generated_text"], "time": time_interval}
