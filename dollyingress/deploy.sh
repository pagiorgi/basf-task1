#!/bin/bash
version=1.0
## TRS start
#create the image
docker build -t giorgip/dolly:ingresstrs$version  --target trs .
#upload image to registry
docker push giorgip/dolly:ingresstrs$version
#deploy in kubernetes
verdeploy=ingresstrs$version
app="dollyitrs"
kubectl apply -f ./kubernetes/namespace.yml
cp ./kubernetes/deploy.yml ./kubernetes/todeploy.yml
sed -i "s/<version>/$verdeploy/" ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
kubectl apply -f ./kubernetes/todeploy.yml
#kubectl delete -f ./kubernetes/todeploy.yml
#Deploy services.yml
cp ./kubernetes/services.yml ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
kubectl apply -f ./kubernetes/todeploy.yml

rm -fr ./kubernetes/todeploy.yml


## TOKE start
version=1.0
#create the image
docker build -t giorgip/dolly:ingresstoke$version  --target toke .
#upload image to registry
docker push giorgip/dolly:ingresstoke$version
#deploy in kubernetes
verdeploy=ingresstoke$version
app="dollyitoke"
kubectl apply -f ./kubernetes/namespace.yml
cp ./kubernetes/deploy.yml ./kubernetes/todeploy.yml
sed -i "s/<version>/$verdeploy/" ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
kubectl apply -f ./kubernetes/todeploy.yml
#kubectl delete -f ./kubernetes/todeploy.yml
#Deploy services.yml
cp ./kubernetes/services.yml ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
kubectl apply -f ./kubernetes/todeploy.yml

rm -fr ./kubernetes/todeploy.yml

# apply ingress

kubectl apply -f ./kubernetes/ingress.yml
#kubectl delete -f ./kubernetes/ingress.yml
