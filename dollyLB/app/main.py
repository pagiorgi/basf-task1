#packages and modules
from fastapi import FastAPI
import torch
import time
from transformers import pipeline
from app.modules.instruct_pipeline import InstructionTextGenerationPipeline
from transformers import AutoModelForCausalLM, AutoTokenizer



# trust_remote_code=True
generate_text_trs = pipeline(model="databricks/dolly-v2-3b",torch_dtype=torch.bfloat16,trust_remote_code=True,device_map="auto") #22 second
#module
tokenizer = AutoTokenizer.from_pretrained("databricks/dolly-v2-3b", padding_side="left")
model = AutoModelForCausalLM.from_pretrained("databricks/dolly-v2-3b", device_map="auto", torch_dtype=torch.bfloat16)
generate_text = InstructionTextGenerationPipeline(model=model, tokenizer=tokenizer)


# API
app = FastAPI()

@app.get("/")
def read_root():
    return {"url request": "http://<host>/<appirequest>/?q='your query'"}


@app.get("/dollytrs/")
def dollytrs(q):
    start = time.time()
    res = generate_text_trs(q)
    end = time.time()
    time_interval = end - start
    return {"query": q, "respons": res[0]["generated_text"], "time": time_interval}


@app.get("/dollytoke/")
def dollytoke(q):
    start = time.time()
    res = generate_text(q)
    end = time.time()
    time_interval = end - start
    return {"query": q, "respons": res[0]["generated_text"], "time": time_interval}