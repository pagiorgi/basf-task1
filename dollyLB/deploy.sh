#!/bin/bash
version=1.0
docker build -t giorgip/dolly:lb$version  --target lb .

#upload image to registry
docker push giorgip/dolly:lb$version
#deploy in kubernetes
verdeploy=lb$version
kubectl apply -f kubernetes/namespace.yml
cp ./kubernetes/deploy.yml ./kubernetes/deploy_todeploy.yml
sed -i "s/<version>/$verdeploy/" ./kubernetes/deploy_todeploy.yml
sed -i "s/<app>/loadbalancer/" ./kubernetes/deploy_todeploy.yml
echo "start "
kubectl apply -f ./kubernetes/deploy_todeploy.yml
rm -fr ./kubernetes/deploy_todeploy.yml

