# basf-task1


## Kubernetes and AI/ML

Deploy the large language model “Dolly v2 3b” as an API on the Kubernetes cluster

## Description
The Dolly v2 documentation shows three ways to use it with python:
1. trust_remote_code = true
2. loaded model and tokenizer
3. LangChain
In this task I will deploy the first two to test the ingress and the LoadBalancer

I have also made three different clearances:
1. Using Ingress (with ingress_controllers)
2. Using LoadBalancer
3. As a web App using shiny with python (I have tested this in my local environment since I need a web browser on the network)


## Before starting
Before starting the deployments, we are going to analyze the resources we have and the resources we are going to need.
### Resources we have
We have a kubernetes cluster deployed in Azure with the following capacity

| nodo                               | cpu | memory(allocable) |
|------------------------------------|-----|-------------------|
| aks-agentpool-40330528-vmss000000  | 4   | 12880736Ki        |
| aks-agentpool-40330528-vmss000001  | 4   | 12880736Ki        |
| aks-agentpool-40330528-vmss000000  | 4   | 12880736Ki        |

This means that we have **4 CPUs** and about **12GB** per node

### How much are we going to need?

Dolly V2 is trained on ~15k instruction/response vailable in these larger models sizes:
dolly-v2-12b, a 12 billion parameter based on pythia-12b.
dolly-v2-7b, a 6.9 billion parameter based on pythia-6.9b.
dolly-v2-3b, a 2.8 billion parameter based on pythia-2.8b.

For our task we will use  dolly-v2-3b 2.8 billion parameter.
```
dolly-v2-3b is a 2.8 billion parameter in 4 cores =12GB
but we going to use torch_dtype=torch.bfloat16
total=12/2 = 6GB /pod
```
Therefore, each pod must reserve at least **6GB** of memory.

## Get Stast

### Application:
I have used *FastAPI* package to deploy python as api using *uvicorn*
You can find the code in the app folder of each directory.
- maintrs.py  --> to implement trust_remote_code as true.
- maintoke.py --> to implement models in local.

### Deploy
To deploy I use a **Dockerfile** that installs the necessary libraries and starts the app.
From the Dockerfile I create an image that I upload to my docker hub repository (this should be in an azure repository)

### Deploy Dolly v2 to kubernetes using ingress
Kubernetes Ingress allows you to flexibly route traffic from outside your Kubernetes cluster to services inside your cluster.

Two pods will be deployed each with its IP cluster service exposing port 80. 
Then an input to route according to the input request

 ![alt tag](./image/ingress.PNG)


#### Clone the repo
We clone the repository so we can execute the installation

```
git clone https://gitlab.com/pagiorgi/basf-task1.git
cd basf-task1/dollyingress/
hmod +x *sh
```
#### Resurcess
Before deploying we edit the resources that the app will have

kuberetes/deploy.yml
```
resources:
            limits:
              cpu: '3'
              memory: 11000Mi
            requests:
              cpu: '3'
              memory: 11000Mi

```
With this configuration we can only start this app once in the cluster since it will retain almost all of its memory. As there are two pods, one will be started on each server and we will have a free node


#### deploy

Run the following command

```
./deploy.sh 
```

What this command does is:
1. Run the docker build to create the image defined in the Dockerfile
2. Upload the image to a Registry
3. Create a namespace
4. Edit the ./kubernetes/deploy.yml file to deploy the app.
5. Apply the yml file
6. Edit the ./kubernetes/services.yml file to create the services.
7. Apply the services
8. Create the ingress.


**note:** steps 3 to 8 can be done in one step

#### test

Check that everything is correct

![alt tag](./image/alldollyingress.PNG)

See that each pod has been raised on a different node as expected, and that it has reserved the indicated  memory and CPU.

![alt tag](./image/nodes.PNG)

Let's test now if this answers

Obtain the IP of the ingress controller which is our entry gateway to the cluster
```
INGGRES_IP=$(kubectl get services ingress-nginx-controller -n ingress-basic -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo $INGGRES_IP
 
```
run the request
What is the difference between nuclear fission and nuclear fusion?
```
curl -v http://$INGGRES_IP/dollytrs/?q="What%20is%20the%20difference%20between%20nuclear%20fission%20and%20nuclear%20fusion?"

```
answer:

```
Connection #0 to host 51.138.230.127 left intact
{"query":"What is the difference between nuclear fission and nuclear fusion?","respons":"Nuclear fusion creates a atomic nucleus without separation of electrons, whereas nuclear fission separates the nuclei to create two charged particles and two neutral particles.","time":354.08333563804626}

```
In the json of the response you can see how long it took to respond. **Almost 6 minutes**

check the pod logs:

![alt tag](./image/dollyingresstrslog.PNG)


Let's ask the other pod now

We will change the question to verify that it goes to the other pod
Run the request 
Who was the last president of Spain??
```
curl -v http://$INGGRES_IP/dollytoke/?q="Who%20was%20the%20last%20president%20of%20Spain?"

```
answer:

```
candidate@hiring-2:~$ curl -L -k http://$INGGRES_IP/dollytoke/?q="Who%20was%20the%20last%20president%20of%20Spain?"
{"query":"Who was the last president of Spain?","respons":"The last president of Spain was Mariano Rajoy.\nAlthough he is an conservative, he is also one of the most open-minded politicians in the European Union. He favors both the full integration of Spain in the European Union and also cooperation with the Union's smallest member states.\nHe was born on 17 June 1946 and he has an educational background in engineering. He studied in the prestigious Institut d'Estudes Politiques in Paris, where he met his wife, María Dolores Pradera.\nSince May 2011, he has been the prime minister of Spain. \nIn office, he moved very quickly and his priority was the resolution of the country's economic crisis. This led to the liberalisation of the labor market, an increase in the minimum wage, and the empowerment of employee organizations.\nUnder his leadership, the unemployment rate fell from a peak of 24% in 2011 to 14.7% in 2017. This figure was considerably lower than the EU average of 21.7%. \nHis government has also made important steps to strengthen the labor code, social security and pensions. \nHe implemented a series of measures against tax evasion, opening over 300 tax havens and arresting some 700 people. In the field of digital economy,","time":1621.031631231308}
 
```
In the json of the response you can see how long it took to respond. **Almost 27 minutes**

#### Delete app
Before starting to deploy the new application we must delete the previous one since we need space


```
 ./deleteall.sh
```
output
```
version=1.0

verdeploy=ingresstrs$version
app="dollyitrs"
cp ./kubernetes/deploy.yml ./kubernetes/todeploy.yml
sed -i "s/<version>/$verdeploy/" ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
#kubectl apply -f ./kubernetes/todeploy.yml
kubectl delete -f ./kubernetes/todeploy.yml
#Deploy services.yml
cp ./kubernetes/services.yml ./kubernetes/todeploy.yml
sed -i "s/<app>/$app/" ./kubernetes/todeploy.yml
echo "start deploy"
kubectl delete -f ./kubernetes/todeploy.yml

rm -fr ./kubernetes/todeploy.yml
```

```
candidate@hiring-2:~/task1/ingress$ kubectl get all  -n dollyingress
No resources found in dollyingress namespace.
```

**NOTE**:It has been necessary to add the following parameters to the entry so that it does not give a waiting time. Since the waiting time per request is one minute and our application takes 30 minutes

```
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "3600"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "3600"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "3600"
```

### Deploy Dolly v2 to kubernetes using LoadBalaner
I deploy an application that is listening on both paths
and I give it 3 replicas, one for each node. In this way the balancer will balance between them.

 ![alt tag](./image/lb.PNG)


#### Clone the repo
We clone the repository so we can execute the installation

```
git clone https://gitlab.com/pagiorgi/basf-task1.git
cd basf-task1/dollyLB
chmod +x *sh
```
#### Resurcess
Before deploying we edit the resources that the app will have

kuberetes/deploy.yml
```
          resources:
            limits:
              cpu: '3'
              memory: 12000Mi
            requests:
              cpu: '3'
              memory: 12000Mi

```
With this configuration, one per node will be started


#### deploy

Run the following command

```
./deploy.sh 
```

What this command does is:
1. Run the docker build to create the image defined in the Dockerfile
2. Upload the image to a Registry
3. Create a namespace
4. Edit the ./kubernetes/deploy.yml file to deploy the app.
5. Apply the yml file

note: steps 3 to 5 can be done in one step

#### test

Check that everything is correct

![alt tag](./image/alldollylb.PNG)

See that each pod has been raised on a different node as expected, and that it has reserved the indicated  memory and CPU.

![alt tag](./image/nodeslb.PNG)

Let's test now if this answers

Obtain the IP of the ingress controller which is our entry gateway to the cluster
```
SERVICE_IP=$(kubectl get services lbloadbalancer -n dollyapp -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo $SERVICE_IP
 
```
run the request
What is the difference between nuclear fission and nuclear fusion?
```
curl -L -k http://$SERVICE_IP/dollytrs/?q="What%20is%20the%20difference%20between%20nuclear%20fission%20and%20nuclear%20fusion?"

```
answer:

```
{"query":"What is the difference between nuclear fission and nuclear fusion?","respons":"Nuclear fusion involves two nuclei combine to produce energy, in contrast to nuclear fission which separates a single atomic nucleus into energy producing parts. When two nuclei come too close together, the combined mass has such a low gravity that it literally breaks apart. This is how nuclear fission occurs.","time":511.05415081977844}

```
In the json of the response you can see how long it took to respond. **Almost 8 minutes**


Let's ask the other pod now

Run the request 
```
curl -L -k http://$SERVICE_IP/dollytoke/?q="What%20is%20the%20difference%20between%20nuclear%20fission%20and%20nuclear%20fusion?"

```
answer:

```
candidate@hiring-2:~/lb/basf-task1/dollyLB$ curl -L -k http://$SERVICE_IP/dollytoke/?q="What%20is%20the%20difference%20between%20nuclear%20fission%20and%20nuclear%20fusion?"

{"query":"What is the difference between nuclear fission and nuclear fusion?","respons":"Nuclear fission occurs when very small particles, or \"fission particles\", split apart material that is a little larger than they are. For example, a small particle might cut an atom in two, separating an atom from the atom itself.\nNuclear fusion occurs when two (or more) atoms join together to form a larger atom, much in the way two halves of a coin become one quarter.\nNuclear fusion is a process of converting Nuclear fission into a more sustainable form of energy. The energy released as a result of fusion is much more than that released by the same amount of fission.\nEfficient conversion of nuclear fission into nuclear fusion requires very specific types of atoms and an environment with certain properties. For example, the proper temperature and pressure must be maintained for a long enough period of time for the fusion to occur.\nNuclear fusion has been identified as the process that created the first stars in the universe. However, current fusion technologies are not at an advanced enough stage to actually generate energy from this process.\nThe development of fusion energy is a very active field of research and many private and governmental organizations are working towards making this a reality. To that end, you can explore the fusion energy website of the fusion for food website for more information.","time":1623.8618195056915}
 
```
In the json of the response you can see how long it took to respond. **Almost 27 minutes**


#### Delete app
Before starting to deploy the new application we must delete the previous one since we need space


```
 ./deleteall.sh
```


### Deploy Dolly v2 in Browser
To deploy this application in a browser I used Shiny + Python.

For this case I have only used the entire model call with trust_remote_code = true
You can find the application at .dollyBrowser/app

To deploy this application we must delete the previous application

```
git clone https://gitlab.com/pagiorgi/basf-task1.git
cd basf-task1/dollyLB
chmod +x *sh
./deleteall.sh
```
#### Resurcess
I am going to give it the minimum resources so that it can coexist with the login application.
```
          resources:
            limits:
              cpu: '1'
              memory: 6000Mi
            requests:
              cpu: '1'
              memory: 6000Mi
```

We go to the web application directory and deploy it

```
cd ../dollyBrowwser
chmod +x *sh
./deploy.sh
```
What this command does is:
1. Run the docker build to create the image defined in the Dockerfile
2. Upload the image to a Registry
3. Create a namespace
4. Apply the yml file ./kubernetes/deploy.sh

#### test

Check that everything is correct

![alt tag](./image/allshinynamespace.PNG)

I have no way to check this because I don't have a browser on this network, so I have tested it on my cluster and it works

![alt tag](./image/shinyweb.PNG)

For a browser on the same network, you should be able to:
```
SERVICE_IP=$(kubectl get services lbdollyshiny -n dollyapp -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo $SERVICE_IP
http://$SERVICE_IP:9090

```

test by curl

```
curl -v http://$SERVICE_IP:9090

```
answer:

```
candidate@hiring-2:~/lb/basf-task1/dollyBrowser$ curl -v http://$SERVICE_IP:9090
*   Trying 20.93.110.125:9090...
* Connected to 20.93.110.125 (20.93.110.125) port 9090 (#0)
> GET / HTTP/1.1
> Host: 20.93.110.125:9090
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< date: Fri, 01 Dec 2023 20:09:42 GMT
< server: uvicorn
< content-length: 1059
< content-type: text/html; charset=utf-8
< 
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <script type="application/html-dependencies">requirejs[2.3.6];jquery[3.6.0];shiny[0.0.1];bootstrap[5.3.1]</script>
    <script src="lib/requirejs-2.3.6/require.min.js"></script>
    <script src="lib/jquery-3.6.0/jquery-3.6.0.min.js"></script>
    <link href="lib/shiny-0.0.1/shiny.min.css" rel="stylesheet"/>
    <script src="lib/shiny-0.0.1/shiny.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="lib/bootstrap-5.3.1/bootstrap.min.css" rel="stylesheet"/>
    <script src="lib/bootstrap-5.3.1/bootstrap.bundle.min.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="shiny-input-textarea form-group shiny-input-container">
        <label class="control-label" id="query-label" for="query">Query</label><textarea id="query" class="form-control" style="width:100%;" placeholder="Enter text"></textarea>
      </div>
      <pre id="txt" class="shiny-text-output noplaceholder"></pre>
    </div>
  </body>
* Connection #0 to host 20.93.110.125 left intact
</html>candidate@hiring-2:~/lb/basf-task1/dollyBrowser$ 

```


