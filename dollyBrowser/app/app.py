from shiny import ui, render, App
import torch
from transformers import pipeline

generate_text = pipeline(model="databricks/dolly-v2-3b", torch_dtype=torch.bfloat16, trust_remote_code=True, device_map="auto")

app_ui = ui.page_fluid(
    ui.input_text_area("query", "Query", placeholder="Enter text"),
    ui.output_text_verbatim("txt"),
)

def server(input, output, session):
    @output
    @render.text
    def txt():
        res = generate_text(input.query())
        return  {res[0]["generated_text"]}

# This is a shiny.App object. It must be named `app`.
app = App(app_ui, server)