#!/bin/bash

#create the image
docker build -t giorgip/dollyshiny  .

#upload image to registry
docker push giorgip/dollyshiny

#deploy in kubernetes

kubectl apply -f ./kubernetes/deploy.yml