wget https://get.helm.sh/helm-v3.13.2-linux-amd64.tar.gz


NAMESPACE=ingress-basic
kubectl create namespace $NAMESPACE
./linux-amd64/helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
./linux-amd64/helm  repo update

./linux-amd64/helm install ingress-nginx ingress-nginx/ingress-nginx \
  --namespace $NAMESPACE \
  --set controller.replicaCount=2  \
  --set controller.nodeSelector."kubernetes\.io/os"=linux \
  --set defaultBackend.nodeSelector."kubernetes\.io/os"=linux \
  --set controller.service.externalTrafficPolicy=Local \
  
kubectl get pods -n ingress-basic
kubectl exec -it  ingress-nginx-controller-75967d99c9-4btdv -n ingress-basic bash

